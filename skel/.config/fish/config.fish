if status is-interactive
    # Commands to run in interactive sessions can go here
end

set -g fish_greeting

neofetch

alias r "ranger"
alias v "vim"
alias ls "exa -al"

alias addup "git add -u"
alias commit "git commit -m"
alias push "git push -u origin main"

alias p "guipower"
