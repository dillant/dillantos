#+title:   Fish shell config
#+property: header-args :tangle config.fish

* Startup
#+begin_src conf 
if status is-interactive
    # Commands to run in interactive sessions can go here
end

set -g fish_greeting

neofetch 
#+end_src

* Alias
#+begin_src conf
alias r "ranger"
alias v "vim"
alias ls "exa -al"
#+end_src

* Alias for git
#+begin_src conf
alias addup "git add -u"
alias commit "git commit -m"
alias push "git push -u origin main"
#+end_src

* Alias for powermenu
#+begin_src conf
alias p "guipower"
#+end_src
