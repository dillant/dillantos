#!/usr/bin/env bash

ln -sf /usr/share/zoneinfo/Europe/London /etc/localtime
hwclock --systohc

sed -i 's/#en_GB.UTF-8 UTF-8/en_GB.UTF-8 UTF-8/g' /etc/locale.gen
touch /etc/locale.conf
echo "LANG=en_GB.UTF-8" > /etc/locale.conf

touch /etc/hostname
echo "dillantos" > /etc/hostname
pacman -S networkmanager
systemctl enable NetworkManager.service

echo "dillant" | passwd --stdin

pacman -S --noconfirm grub efibootmgr os-prober dosfstools mtools

mkdir /boot/EFI
grub-install --target=x86_64-efi --efi-directory=/boot/EFI --bootloader-id=GRUB --recheck

grub-mkconfig -o /boot/grub/grub.cfg

exit
