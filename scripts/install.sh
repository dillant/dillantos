#!/usr/bin/env sh

export NEWT_COLORS='
       root=,red
'

disk="vda"

timedatectl

(echo g; echo n; echo ; echo ; echo +1G; echo n; echo ; echo ; echo +4G; echo n; echo ; echo ; echo ; echo t; echo 1; echo 1; echo t; echo 2; echo 19; echo w;) | fdisk /dev/$disk

mkfs.fat -F32 /dev/$disk"1"
mkswap /dev/$disk"2"
mkfs.ext4 /dev/$disk"3"

mount /dev/$disk"3" /mnt
mount --mkdir /dev/$disk"1" /mnt/boot
swapon /dev/$disk"2"

reflector --country 'Germany'

pacstrap -K /mnt base linux linux-firmware

genfstab -U /mnt >> /mnt/etc/fstab

mkdir /mnt/etc/dillantos
git clone https://gitlab.com/dillant/dillantos.git /mnt/etc/dillantos

arch-chroot /mnt /bin/bash /etc/dillantos/scripts/installp2.sh

umount -l /mnt

shutdown now
