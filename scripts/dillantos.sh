#!/usr/bin/env bash

if [[ $(id -u) == 0 ]]; then
    echo "You cannot run this script as root as it makes chnages to the user!!!"
    exit 1
fi

export NEWT_COLORS='
       root=,red
'

whiptail --title "CLIHELPER" --yesno "This is still in beta and my contain bugs that could be fatal to your system so take this with caution before running. WE ARE NOT RESPONISBLE FOR ANY DAMAGE TO YOU SYSTEM. Make sure to be at your computer as you will be prompted a few times, Would you like to continue?" 0 0 3>&1 1>&2 2>&3
if [[ $? == 1 ]]; then
    exit 1
fi

echo "##########################"
echo "#    Updating system     #"
echo "##########################"
sleep 1
sudo pacman -Syu --noconfirm

echo "#################################"
echo "#   Applying DillantOS Configs  #"
echo "#################################"
sleep 1
rm -rf ~/.config/*

cp -r ~/dillantos/skel/.config/* ~/.config/
cp -r ~/dillantos/skel/wallpapers ~/

echo "#####################"
echo "#  Installing Apps  #"
echo "#####################"
sleep 1
cd ~/dillantos
sudo pacman -S --needed --noconfirm $(comm -12 <(pacman -Slq | sort) <(sort pkglist.txt))

sudo ln -s /usr/lib/libalpm.so.14.0.0 /usr/lib/libalpm.so.13

echo "##################################"
echo "#  Setting up custom SDDM theme  #"
echo "##################################"
sleep 1

paru -S --noconfirm sddm-sugar-dark

sudo cp -r ~/dillantos/configs/sugar-dark /usr/share/sddm/themes/

if [[ -f /etc/sddm.conf.d/kde_settings.conf ]]; then
    sudo rm /etc/sddm.conf.d/kde_settings.conf
fi

if [[ ! -d /etc/sddm.conf.d ]]; then
    sudo mkdir /etc/sddm.conf.d
fi

sudo cp ~/dillantos/configs/kde_settings.conf /etc/sddm.conf.d/kde_settings.conf

sudo systemctl enable sddm.service

echo "#############################"
echo "#  Setting up Herbstluftwm  #"
echo "#############################"
sleep 1
sudo pacman -S --needed --noconfirm xorg

cd ~/
git clone https://gitlab.com/dillant/herbstluftwm-config.git
mv herbstluftwm-config ~/.config/herbstluftwm

echo "######################################"
echo "#  Setting up DillantOS Welcome App  #"
echo "######################################"
sleep 1
echo "~/dillantos/scripts/./welcome.sh" >> ~/.config/herbstluftwm/autostart

sudo cp ~/dillantos/scripts/welcome.sh /usr/local/bin/dwelcome
sudo cp ~/dillantos/configs/dillantos.desktop /usr/share/applications/

echo "###########################"
echo "#  Setting up Doom Emacs  #"
echo "###########################"
sleep 1

git clone --depth 1 --single-branch https://github.com/doomemacs/doomemacs ~/.config/emacs
~/.config/emacs/bin/doom install

~/.config/emacs/bin/doom sync

echo "#############################"
echo "#  Setting up Virt-Manager  #"
echo "#############################"
sleep 1
sudo usermod -a -G libvirt $USER
sudo usermod -a -G libvirt-qemu $USER

echo "##################################"
echo "#  Installing Custom Power Menu  #"
echo "##################################"
sleep 1

cd ~/dillantos
git clone https://gitlab.com/dillant/simple-power-menu
sudo cp ~/dillantos/simple-power-menu/guipower.sh /usr/local/bin/guipower

echo "###############"
echo "#  Rebooting  #"
echo "###############"
sleep 1
reboot
