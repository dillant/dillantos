#!/usr/bin/env bash

x=0
while [[ $x != 1 ]] do
      app=$(yad --title "Welcome to Dillant OS" --height="500" --width="500" \
          --text "Welcome to DillantOS the distro that is about tiling windows and backups!!!" \
          --list \
          --column=Action \
          "Update System" "See Keybindings" "Stop app from opening on next boot" "Website Of Creator")

      if [[ $? == 1 ]]; then
          x=1
      elif [[ $app == "Update System|" ]]; then
          alacritty -e sudo pacman -Syu
      elif [[ $app == "Stop app from opening on next boot|" ]]; then
          sed -i 's#~/dillantos/scripts/./welcome.sh##g' ~/.config/herbstluftwm/autostart
      elif [[ $app == "See Keybindings|" ]]; then
        binds=$(cat ~/dillantos/scripts/keys.txt)
        yad --title "Keybinds" --height="300" \
          --text "$binds"
      elif [[ $app == "Website Of Creator|" ]]; then
        firefox https://thedillonking.com
      fi
done
